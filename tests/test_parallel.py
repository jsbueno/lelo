# coding: utf-8
# Author: João S. O. Bueno
from lelo import parallel


import sys, os

import unittest


def test_can_import():
    assert __import__("lelo")

def test_can_execute_func_out_of_process():
    from multiprocessing import Queue, Process
    from lelo._lelo import _xecuter
    queue = Queue(1)

    def sum_(a, b):
        return a + b

    proc  = Process(target=_xecuter,
        args=(queue, sum_, (2, 3), {}))
    proc.start()

    assert queue.get() == 5

def test_proc_wrapper_works():

    from lelo._lelo import Wrapper, LazyCallWrapper
    def sum_(a,b):
        return a + b

    wrapped = Wrapper(sum_)
    result = wrapped(2,3)
    assert type(result) == LazyCallWrapper
    assert result._value == 5

def test_lazy_class_factory():
    from lelo._lelo import MetaParallel
    X = MetaParallel("X", (object,), {})
    x = X()
    # check for operator access and work
    object.__setattr__(x, "_value", 10)
    assert x + 0 == 10
    # check for attribute access
    object.__setattr__(x, "_value", {"y": 10})
    assert list(x.keys()) == ["y"]

def test_parallel_function_creation():
    from types import MethodType
    @parallel
    def soma(a, b):
        return a + b
    x = getattr(soma, "__call__")
    assert x.__class__ == MethodType

def test_parallel_execution():
    @parallel
    def soma(a, b):
        return a + b
    result = soma(2,3)
    assert result == 5

def test_async_exec():
    import time
    try:
        from urllib import urlopen
    except ImportError:
        from urllib.request import urlopen

    url = "http://example.com"


    def retr_html(url):
        return urlopen(url).read()

    fast = parallel(retr_html)
    t0 = time.time()
    res_0 = fast(url)
    t0 = time.time() - t0

    t1 = time.time()
    res_1 = retr_html(url)
    t1 = time.time() - t1

    assert t0 < t1 / 10
    assert len(res_0) == len(res_1)

def test_async_iterator():
    from types import GeneratorType
    @parallel
    def fibonacci(limit):
        n = 1
        n1 = 0
        counter = 0
        while counter < limit:
            n, n1 = n1, n + n1
            counter += 1
            yield n1

    assert list(fibonacci(10)) == [1, 1, 2, 3, 5, 8, 13, 21, 34, 55]

