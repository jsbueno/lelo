The idea is to have an ubber simple decorator
which simply defers the calculation of any function
or method off process.

The decorated function returns a lazy proxy object,
which will just attempt to retrieve the actual
return value of the function when it is about to
be used in any Python expression.

Currently, the only public member in the
project is the "parallel" decorator, which is used just as:

```
#!python

@parallel
def retr_url(url):
    return urllib.urlopen(url).read()
```

-And voilá - upon calling the above "retr_url",
execution simply proceeds, while Python multiprocessing
creates another process to actually download the url
contents.

Unlike Python's 3.2 concurrent.futures, there is no
setup needed - just decorate your function
and you are all set-up to go.


# TODO:
Explict "future"'s instead of just transparent magic proxies,
worker pool configuration and management
